import {
  rjv_default,
  rjv_grey,
  rjve_light,
  rjve_dark,
} from "./base16/rjv-themes";
import constants from "./styleConstants";
import { createStyling } from "react-base16-styling";

const colorMap = theme => ({
  backgroundColor: theme.base00,
  ellipsisColor: theme.base09,
  braceColor: theme.base05,
  expandedIcon: theme.base0C,
  collapsedIcon: theme.base0C,
  keyColor: theme.base05,
  arrayKeyColor: theme.base0C,
  objectSize: theme.base04,
  copyToClipboard: theme.base0C,
  copyToClipboardCheck: theme.base0C,
  objectBorder: theme.base02,
  dataTypes: {
    boolean: theme.base05,
    date: theme.base05,
    float: theme.base09,
    function: theme.base09,
    integer: theme.base09,
    string: theme.base0B,
    nan: theme.base05,
    null: theme.base0A,
    undefined: theme.base0A,
    regexp: theme.base0A,
    background: theme.base00,
    pycomplex: theme.base09,
    pykeyword: theme.base0E,
    pygeneral: theme.base05,
    pybytes: theme.base0B,
    pycallable: theme.base05,
    pybuiltin: theme.base0A,
    pyexception: theme.base06,
    pywarning: theme.base07,
    pydatetime: theme.base05,
  },
  editVariable: {
    editIcon: theme.base0E,
    cancelIcon: theme.base06,
    removeIcon: theme.base06,
    addIcon: theme.base0B,
    checkIcon: theme.base0B,
    background: theme.base01,
    color: theme.base0A,
    border: theme.base07,
  },
  addKeyModal: {
    background: theme.base05,
    border: theme.base04,
    color: theme.base0A,
    labelColor: theme.base01,
  },
  validationFailure: {
    background: theme.base09,
    iconColor: theme.base01,
    fontColor: theme.base01,
  },
});

const getDefaultThemeStyling = theme => {
  const colors = colorMap(theme);

  return {
    "react-json-view": {
      fontFamily: constants.globalFontFamily,
      cursor: constants.globalCursor,
      backgroundColor: colors.backgroundColor,
      position: "relative",
    },
    ellipsis: {
      display: "inline-block",
      color: colors.ellipsisColor,
      fontSize: constants.ellipsisFontSize,
      lineHeight: constants.ellipsisLineHeight,
      cursor: constants.ellipsisCursor,
    },
    "brace-row": {
      display: "inline-block",
      cursor: "pointer",
    },
    brace: {
      display: "inline-block",
      cursor: constants.braceCursor,
      fontWeight: constants.braceFontWeight,
      color: colors.braceColor,
    },
    "expanded-icon": {
      color: colors.expandedIcon,
    },
    "collapsed-icon": {
      color: colors.collapsedIcon,
    },
    colon: {
      display: "inline-block",
      margin: constants.keyMargin,
      color: colors.keyColor,
      verticalAlign: "top",
    },
    objectKeyVal: (component, variable_style) => {
      return {
        style: {
          paddingTop: constants.keyValPaddingTop,
          paddingRight: constants.keyValPaddingRight,
          paddingBottom: constants.keyValPaddingBottom,
          borderLeft: constants.keyValBorderLeft + " " + colors.objectBorder,
          ":hover": {
            paddingLeft: variable_style.paddingLeft - 1 + "px",
            borderLeft: constants.keyValBorderHover + " " + colors.objectBorder,
          },
          ...variable_style,
        },
      };
    },
    "object-key-val-no-border": {
      padding: constants.keyValPadding,
    },
    "pushed-content": {
      marginLeft: constants.pushedContentMarginLeft,
    },
    variableValue: (component, variable_style) => {
      return {
        style: {
          display: "inline-block",
          paddingRight: constants.variableValuePaddingRight,
          position: "relative",
          ...variable_style,
        },
      };
    },
    "object-name": {
      display: "inline-block",
      color: colors.keyColor,
      letterSpacing: constants.keyLetterSpacing,
      fontStyle: constants.keyFontStyle,
      verticalAlign: constants.keyVerticalAlign,
      opacity: constants.keyOpacity,
      ":hover": {
        opacity: constants.keyOpacityHover,
      },
    },
    "array-key": {
      display: "inline-block",
      color: colors.arrayKeyColor,
      letterSpacing: constants.keyLetterSpacing,
      fontStyle: constants.keyFontStyle,
      verticalAlign: constants.keyVerticalAlign,
      opacity: constants.keyOpacity,
      ":hover": {
        opacity: constants.keyOpacityHover,
      },
    },
    "object-size": {
      color: colors.objectSize,
      borderRadius: constants.objectSizeBorderRadius,
      fontStyle: constants.objectSizeFontStyle,
      margin: constants.objectSizeMargin,
      cursor: "default",
    },
    "data-type-label": {
      fontSize: constants.dataTypeFontSize,
      marginRight: constants.dataTypeMarginRight,
      lineHeight: constants.dataTypeLineHeight,
      opacity: constants.datatypeOpacity,
      verticalAlign: "top",
    },
    boolean: {
      display: "inline-block",
      color: colors.dataTypes.boolean,
    },
    date: {
      display: "inline-block",
      color: colors.dataTypes.date,
    },
    "date-value": {
      marginLeft: constants.dateValueMarginLeft,
    },
    float: {
      display: "inline-block",
      color: colors.dataTypes.float,
    },
    function: {
      display: "inline-block",
      color: colors.dataTypes["function"],
      cursor: "pointer",
      whiteSpace: "pre-line",
    },
    "function-value": {
      fontStyle: "italic",
    },
    integer: {
      display: "inline-block",
      color: colors.dataTypes.integer,
    },
    string: {
      display: "inline-block",
      color: colors.dataTypes.string,
    },
    nan: {
      display: "inline-block",
      color: colors.dataTypes.nan,
      fontSize: constants.nanFontSize,
      fontWeight: constants.nanFontWeight,
      backgroundColor: colors.dataTypes.background,
      padding: constants.nanPadding,
      borderRadius: constants.nanBorderRadius,
    },
    null: {
      display: "inline-block",
      color: colors.dataTypes.null,
      fontSize: constants.nullFontSize,
      fontWeight: constants.nullFontWeight,
      backgroundColor: colors.dataTypes.background,
      padding: constants.nullPadding,
      borderRadius: constants.nullBorderRadius,
    },

    undefined: {
      display: "inline-block",
      color: colors.dataTypes.undefined,
      fontSize: constants.undefinedFontSize,
      padding: constants.undefinedPadding,
      borderRadius: constants.undefinedBorderRadius,
      backgroundColor: colors.dataTypes.background,
    },
    regexp: {
      display: "inline-block",
      color: colors.dataTypes.regexp,
    },
    "copy-to-clipboard": {
      cursor: constants.clipboardCursor,
    },
    "copy-icon": {
      color: colors.copyToClipboard,
      fontSize: constants.iconFontSize,
      marginRight: constants.iconMarginRight,
      verticalAlign: "top",
    },
    "copy-icon-copied": {
      color: colors.copyToClipboardCheck,
      marginLeft: constants.clipboardCheckMarginLeft,
    },
    "array-group-meta-data": {
      display: "inline-block",
      padding: constants.arrayGroupMetaPadding,
    },
    "object-meta-data": {
      display: "inline-block",
      padding: constants.metaDataPadding,
    },
    "icon-container": {
      display: "inline-block",
      width: constants.iconContainerWidth,
    },
    tooltip: {
      padding: constants.tooltipPadding,
    },
    removeVarIcon: {
      verticalAlign: "top",
      display: "inline-block",
      color: colors.editVariable.removeIcon,
      cursor: constants.iconCursor,
      fontSize: constants.iconFontSize,
      marginRight: constants.iconMarginRight,
    },
    addVarIcon: {
      verticalAlign: "top",
      display: "inline-block",
      color: colors.editVariable.addIcon,
      cursor: constants.iconCursor,
      fontSize: constants.iconFontSize,
      marginRight: constants.iconMarginRight,
    },
    editVarIcon: {
      verticalAlign: "top",
      display: "inline-block",
      color: colors.editVariable.editIcon,
      cursor: constants.iconCursor,
      fontSize: constants.iconFontSize,
      marginRight: constants.iconMarginRight,
    },
    "edit-icon-container": {
      display: "inline-block",
      verticalAlign: "top",
    },
    "check-icon": {
      display: "inline-block",
      cursor: constants.iconCursor,
      color: colors.editVariable.checkIcon,
      fontSize: constants.iconFontSize,
      paddingRight: constants.iconPaddingRight,
    },
    "cancel-icon": {
      display: "inline-block",
      cursor: constants.iconCursor,
      color: colors.editVariable.cancelIcon,
      fontSize: constants.iconFontSize,
      paddingRight: constants.iconPaddingRight,
    },
    "edit-input": {
      display: "inline-block",
      minHeight: constants.editInputHeight,
      minWidth: constants.editInputMinWidth,
      borderRadius: constants.editInputBorderRadius,
      backgroundColor: colors.editVariable.background,
      color: colors.editVariable.color,
      padding: constants.editInputPadding,
      marginRight: constants.editInputMarginRight,
      fontFamily: constants.editInputFontFamily,
    },
    "detected-row": {
      paddingTop: constants.detectedRowPaddingTop,
    },
    "key-modal-request": {
      position: constants.addKeyCoverPosition,
      top: constants.addKeyCoverPositionPx,
      left: constants.addKeyCoverPositionPx,
      right: constants.addKeyCoverPositionPx,
      bottom: constants.addKeyCoverPositionPx,
      backgroundColor: constants.addKeyCoverBackground,
    },
    "key-modal": {
      width: constants.addKeyModalWidth,
      backgroundColor: colors.addKeyModal.background,
      marginLeft: constants.addKeyModalMargin,
      marginRight: constants.addKeyModalMargin,
      padding: constants.addKeyModalPadding,
      borderRadius: constants.addKeyModalRadius,
      marginTop: "15px",
      position: "relative",
    },
    "key-modal-label": {
      color: colors.addKeyModal.labelColor,
      marginLeft: "2px",
      marginBottom: "5px",
      fontSize: "11px",
    },
    "key-modal-input-container": {
      overflow: "hidden",
    },
    "key-modal-input": {
      width: "100%",
      padding: "3px 6px",
      fontFamily: "monospace",
      color: colors.addKeyModal.color,
      border: "none",
      boxSizing: "border-box",
      borderRadius: "2px",
    },
    "key-modal-cancel": {
      backgroundColor: colors.editVariable.removeIcon,
      position: "absolute",
      top: "0px",
      right: "0px",
      borderRadius: "0px 3px 0px 3px",
      cursor: "pointer",
    },
    "key-modal-cancel-icon": {
      color: colors.addKeyModal.labelColor,
      fontSize: constants.iconFontSize,
      transform: "rotate(45deg)",
    },
    "key-modal-submit": {
      color: colors.editVariable.addIcon,
      fontSize: constants.iconFontSize,
      position: "absolute",
      right: "2px",
      top: "3px",
      cursor: "pointer",
    },
    "function-ellipsis": {
      display: "inline-block",
      color: colors.ellipsisColor,
      fontSize: constants.ellipsisFontSize,
      lineHeight: constants.ellipsisLineHeight,
      cursor: constants.ellipsisCursor,
    },
    "validation-failure": {
      float: "right",
      padding: "3px 6px",
      borderRadius: "2px",
      cursor: "pointer",
      color: colors.validationFailure.fontColor,
      backgroundColor: colors.validationFailure.background,
    },
    "validation-failure-label": {
      marginRight: "6px",
    },
    "validation-failure-clear": {
      position: "relative",
      verticalAlign: "top",
      cursor: "pointer",
      color: colors.validationFailure.iconColor,
      fontSize: constants.iconFontSize,
      transform: "rotate(45deg)",
    },
    pycomplex: {
      display: "inline-block",
      color: colors.dataTypes.pycomplex,
    },
    pykeyword: {
      display: "inline-block",
      color: colors.dataTypes.pykeyword,
    },
    "pykeyword-value": {
      display: "inline-block",
      color: colors.dataTypes.pykeyword,
      fontStyle: "italic",
      fontWeight: "bold",
    },
    pybuiltin: {
      display: "inline-block",
      color: colors.dataTypes.pybuiltin,
    },
    "pybuiltin-value": {
      display: "inline-block",
      color: colors.dataTypes.pybuiltin,
      fontStyle: "italic",
    },
    pygeneral: {
      display: "inline-block",
      color: colors.dataTypes.pygeneral,
    },
    "pygeneral-value": {
      display: "inline-block",
      color: colors.dataTypes.pygeneral,
      fontStyle: "italic",
    },
    pybytes: {
      display: "inline-block",
      color: colors.dataTypes.pybytes,
    },
    pycallable: {
      display: "inline-block",
      color: colors.dataTypes.pycallable,
    },
    "pycallable-value": {
      display: "inline-block",
      color: colors.dataTypes.pycallable,
      fontStyle: "italic",
    },
    pyexception: {
      display: "inline-block",
      color: colors.dataTypes.pyexception,
      fontStyle: "italic",
      fontWeight: "bold",
    },
    pywarning: {
      display: "inline-block",
      color: colors.dataTypes.pywarning,
      fontStyle: "italic",
      fontWeight: "bold",
    },
    pydatetime: {
      display: "inline-block",
      color: colors.dataTypes.pydatetime,
    },
  };
};

const getStyle = theme => {
  let rjv_theme = rjve_light;

  switch (theme) {
    case "dark":
      rjv_theme = rjve_dark;
      break;
    case false:
      rjv_theme = rjv_grey;
      break;
    case "none":
      rjv_theme = rjv_grey;
      break;
    case "rjv-default":
      rjv_theme = rjv_default;
      break;
  }

  return createStyling(getDefaultThemeStyling, { defaultBase16: rjv_theme })(
    theme
  );
};

export default function style(theme, component, args) {
  if (!theme) {
    console.error("theme has not been set");
  }

  return getStyle(theme)(component, args);
}
