import React from "react";
import DataTypeLabel from "./DataTypeLabel";

//theme
import Theme from "./../../themes/getStyle";

export default class extends React.PureComponent {
    render() {
        const type_name = "type";
        const { props } = this;

        return (
            <div {...Theme(props.theme, "pybuiltin")}>
                <DataTypeLabel type_name={type_name} {...props} />
                <span {...Theme(props.theme, "pybuiltin-value")}>
                    {props.value}
                </span>
            </div>
        );
    }
}
