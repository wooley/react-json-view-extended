import React from "react";
import DataTypeLabel from "./DataTypeLabel";

//theme
import Theme from "./../../themes/getStyle";

export default class extends React.PureComponent {
  render() {
    const type_name = "exception";
    const { props } = this;

    return <div {...Theme(props.theme, "pywarning")}>{props.value.type}</div>;
  }
}
