import React from "react";
import DataTypeLabel from "./DataTypeLabel";

//theme
import Theme from "./../../themes/getStyle";

export default class extends React.PureComponent {
    render() {
        const { props } = this;
        const type_name = "bytes";
        const v = props.value.split("b'")[1].split("'")[0];

        return (
            <div {...Theme(props.theme, "pybytes")}>
                <DataTypeLabel type_name={type_name} {...props} />
                <span {...Theme(props.theme, "pykeyword-value")}>b</span>
                {`'${v}'`}
            </div>
        );
    }
}
