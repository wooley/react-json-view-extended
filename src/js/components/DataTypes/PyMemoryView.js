import React from "react";
import DataTypeLabel from "./DataTypeLabel";
import { toType } from "./../../helpers/util";

//theme
import Theme from "./../../themes/getStyle";

//attribute store for storing collapsed state
import AttributeStore from "./../../stores/ObjectAttributes";

export default class extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            collapsed: AttributeStore.get(
                props.rjvId,
                props.namespace,
                "collapsed",
                true
            ),
        };
    }

    toggleCollapsed = () => {
        this.setState(
            {
                collapsed: !this.state.collapsed,
            },
            () => {
                AttributeStore.set(
                    this.props.rjvId,
                    this.props.namespace,
                    "collapsed",
                    this.state.collapsed
                );
            }
        );
    };

    render() {
        const type_name = "memoryview";
        const { collapsed } = this.state;
        const { props } = this;
        const { theme } = props;
        let { value } = props;
        var v_;

        const header = (
            <span {...Theme(theme, "pybuiltin-value")}>
                {`MemoryView(${value.b})  `}
            </span>
        );

        if (this.state.collapsed) {
            v_ = (
                <span>
                    <span> ...</span>
                </span>
            );
        } else {
            v_ = (
                <div {...Theme(theme, "pybuiltin-value")}>{`  [${
                    value.l
                }]`}</div>
            );
        }

        return (
            <div {...Theme(theme, "pybuiltin")} onClick={this.toggleCollapsed}>
                <DataTypeLabel type_name={type_name} {...props} />
                {header}
                {v_}
            </div>
        );
    }
}
