import React from "react";
import DataTypeLabel from "./DataTypeLabel";

//theme
import Theme from "./../../themes/getStyle";

export default class extends React.PureComponent {
    render() {
        const { props } = this;
        const type_name = props.value.type;
        return (
            <div {...Theme(props.theme, "pygeneral")}>
                <DataTypeLabel type_name={type_name} {...props} />
                <div {...Theme(props.theme, "pygeneral-value")}>
                    {props.value.value.split("\n").map((g, ii) => (
                        <div key={`k${ii}`}>{g}</div>
                    ))}
                </div>
            </div>
        );
    }
}
