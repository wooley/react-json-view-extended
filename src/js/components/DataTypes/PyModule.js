import React from "react";
import DataTypeLabel from "./DataTypeLabel";
import { toType } from "./../../helpers/util";

//theme
import Theme from "./../../themes/getStyle";

//attribute store for storing collapsed state
import AttributeStore from "./../../stores/ObjectAttributes";

export default class extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            collapsed: AttributeStore.get(
                props.rjvId,
                props.namespace,
                "collapsed",
                true
            ),
        };
    }

    toggleCollapsed = () => {
        this.setState(
            {
                collapsed: !this.state.collapsed,
            },
            () => {
                AttributeStore.set(
                    this.props.rjvId,
                    this.props.namespace,
                    "collapsed",
                    this.state.collapsed
                );
            }
        );
    };

    render() {
        const type_name = "module";
        const { collapsed } = this.state;
        const { props } = this;
        const { theme } = props;
        let { value } = props;
        var v_;

        if (this.state.collapsed) {
            v_ = (
                <span>
                    <b>{value.n}</b>
                    <span> ...</span>
                </span>
            );
        } else {
            v_ = (
                <span>
                    <b>{value.n}</b>
                    <div>
                        {value.doc.split("\n").map((v, ii) => (
                            <div key={`k${ii}`}>{v}</div>
                        ))}
                    </div>
                </span>
            );
        }

        return (
            <div {...Theme(theme, "pygeneral")} onClick={this.toggleCollapsed}>
                <DataTypeLabel type_name={type_name} {...props} />
                {v_}
            </div>
        );
    }
}
