import React from "react";
import DataTypeLabel from "./DataTypeLabel";

//theme
import Theme from "./../../themes/getStyle";

export default class extends React.PureComponent {
    render() {
        const type_name = "range";
        const { props } = this;
        return (
            <div {...Theme(props.theme, "pygeneral")}>
                <DataTypeLabel type_name={type_name} {...props} />
                <span {...Theme(props.theme, "pygeneral-value")}>
                    {props.value}
                </span>
            </div>
        );
    }
}
