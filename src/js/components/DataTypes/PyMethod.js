import React from "react";
import DataTypeLabel from "./DataTypeLabel";

//theme
import Theme from "./../../themes/getStyle";

export default class extends React.PureComponent {
    render() {
        const { props } = this;
        const type_name = "method";
        return (
            <div {...Theme(props.theme, "pycallable")}>
                <DataTypeLabel type_name={type_name} {...props} />
                <span {...Theme(props.theme, "pycallable-value")}>
                    {props.value.s.split("\n").map((v, ii) => (
                        <div key={`k${ii}`}>{v}</div>
                    ))}
                </span>
            </div>
        );
    }
}
