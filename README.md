# `react-json-view-extended`

This fork extends to excellent [**`react-json-view`**](https://mac-s-g.github.io/react-json-view/demo/dist/) module to accept data types that are beyond the purview of strict JSON.

## Datatypes

### Existing

- Boolean
- Date
- Float
- Function (for javascript functions)
- Integer
- NaN
- Null
- Object
- Regexp
- String
- Undefined

### New

- Bool
- Complex
- Str
- Float
- Int
- NoneType
- Range
- Code
- Iterator
- ContextManager
- UnknownPyType
- UnhandledType
- ByteArray
- Bytes
- Callable
- Method
- MemoryView

* Type
* Ellipsis
* NotImplemented
* Exception
* Warning
* Module
* PandasSeries
* PandasDataFrame
* Figure

- Datetime

- List
- PySet
- Tuple
- Dict
- FrozenSet

## How to add new type

1. Create new component in `./src/js/Components/...`
1. Ensure component is exported in `./src/js/Components/Datatypes.js`
1. Add theming to component in `./src/js/themes/getStyle.js`
1. Import and add as switch to `getValue` method in `./src/js/Components/VariableEditor.js`

### Others to handle

- Braces on Object.js at `getBraceStart` and `render`.

### To be understood

- How does flux work here?
- What is happening in `Object.getState` with parentType and type?
